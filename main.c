/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "fonts.h"
#include "ssd1306.h"
#include <string.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define ADC_BUF_LEN 2048

#define TRESHOLD_DIFFERENCE 50
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim2;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
uint16_t adc_buf[ADC_BUF_LEN];

double joules = 0.0;
// test variable to see it time is adding up correctly
double workTime = 0;

// Napetost s katero je napajan
double inputVoltage = 4.75;



int standby_value = 0;

int battery_status = 75;

// if 1 the system will do automatic calibration and will set variable back to 0
int calibration = 1;



//bool timer_overflow;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM2_Init(void);
static void MX_I2C1_Init(void);
static void MX_USART2_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

char Rx_data[1];
char Rx_data_buffer[500];
uint8_t Rx_data_index = 0;

static void drawAnalogReading(){
	SSD1306_DrawFilledRectangle(0, 0, 128, 18, 0);
	SSD1306_DrawFilledRectangle(0, 20, 128, 18, 0);
	SSD1306_DrawFilledRectangle(0, 40, 128, 18, 0);

	if(calibration != 0){
		SSD1306_GotoXY(0,0);
		char msg_char[] = "Calibrating";
		SSD1306_Puts(msg_char,&Font_11x18, 1);
		SSD1306_UpdateScreen();
		return;
	}

	char j_char[10];

	sprintf(j_char, "%.6f", joules);
	SSD1306_GotoXY(0,0);
	j_char[8] = 'J';
	j_char[9] = 0;
	SSD1306_Puts(j_char,&Font_11x18, 1);

	char t_char[10];

	sprintf(t_char, "%.6f", workTime);
	SSD1306_GotoXY(0,20);
	t_char[8] = 's';
	t_char[9] = 0;
	SSD1306_Puts(t_char,&Font_11x18, 1);


	char v_char[8];
	sprintf(v_char, "%.4f", inputVoltage);
	SSD1306_GotoXY(0,40);
	v_char[6] = 'V';
	v_char[7] = 0;
	SSD1306_Puts(v_char,&Font_11x18, 1);

	SSD1306_UpdateScreen();

}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_TIM2_Init();
  MX_I2C1_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */
  SSD1306_Init();
  HAL_ADC_Start_DMA(&hadc1, (uint32_t*)adc_buf, ADC_BUF_LEN);
  HAL_TIM_Base_Start_IT(&htim2);

  HAL_UART_Receive_IT(&huart2, Rx_data, 1);

  //__HAL_UART_ENABLE_IT(&huart1, UART_IT_RXNE);
  //drawBattery();


  //timer_overflow = false;
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  while (1)
  {
	  drawAnalogReading();
	  HAL_Delay(50);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV4;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV8;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */

  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 400000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 144;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 65535;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 9600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12|GPIO_PIN_13, GPIO_PIN_RESET);

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PB12 PB13 */
  GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

// pove od kje do kje bufferju racuna porabo energije
void calculate_consumption(int start, int stop, double sampleTime){
	double U2;
	double I;
	double P;

	int worktime_counter = 0;

	if(calibration == 1){
		int min = adc_buf[start];
		int max = adc_buf[start];

		standby_value = 0;

		for(int i = start; i < stop; i++){
			if(adc_buf[i] < min) min = adc_buf[i];
			if(adc_buf[i] > max) max = adc_buf[i];
			standby_value += adc_buf[i];
		}

		if(max - min > 100) return;

		standby_value = standby_value / (double)(ADC_BUF_LEN / 2.0);
		inputVoltage = standby_value / 4095.0 * 5.0 + 0.06;
		//inputVoltage = 4.73;
		calibration = 0;

	}


	for(int i = start; i < stop; i++){
	if ((standby_value - adc_buf[i]) < TRESHOLD_DIFFERENCE) continue;

	// convert value to voltage U2
	U2 = (adc_buf[i] / 4095.0) * 5.0 + 0.06;

	// calculate current
	I = (inputVoltage - U2) / 1.35;

	// calculate power
	P = U2 * I;

	// multiply power by time between samples in seconds
	joules += (P * sampleTime);

	worktime_counter+=1;
	}

	workTime = workTime + ((double)worktime_counter * sampleTime);
}

void HAL_ADC_ConvHalfCpltCallback(ADC_HandleTypeDef* hadc) {
	volatile uint32_t timerValue = TIM2->CNT;
	TIM2->CNT = 0;
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET);

	// Start processing flag
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_SET);

	double sampleTime = timerValue / 1024.0 * 2.0 / 1000000.0;
	calculate_consumption(0, 1024, sampleTime);

	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_RESET);
}

// Called when buffer is completely filled
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc) {
	volatile uint32_t timerValue = TIM2->CNT;
	TIM2->CNT = 0;

	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);

	// Start processing flag
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_SET);

	double sampleTime = timerValue / 1024.0 * 2.0 / 1000000.0;
	calculate_consumption(1024, 2047, sampleTime);
	
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_RESET);
}


void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* htim)
{
    HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);
	//timer_overflow = true;
}

  //  creating a buffer of 10 bytes

int getIntLength(int number) {
    int length = 0;

    if (number == 0) {
        return 1; 
    }

    if (number < 0) {
        length++; 
        number = -number; 
    }

    while (number != 0) {
        length++;
        number /= 10;
    }

    return length;
}

// Function to convert a double to a char array
void doubleToCharArray(double value, char *buffer, size_t bufferSize) {
    int precision = 6; // Stevilo decimalnih mest

    // Handle negative numbers
    if (value < 0) {
        if (bufferSize < 2) {
            return;
        }
        *buffer++ = '-';
        bufferSize--;
        value = -value;
    }

    int intPart = (int)value;
    double decimalPart = value - intPart;

    int intLen = getIntLength(intPart);
    if (intLen >= bufferSize) {
        return;
    }

    for (int i = intLen - 1; i >= 0; i--) {
        buffer[i] = '0' + intPart % 10;
        intPart /= 10;
    }

    buffer += intLen;
    bufferSize -= intLen;

    // Doda decimalno piko
    if (precision > 0 && bufferSize > 1) {
        *buffer++ = '.';
        bufferSize--;
    }

    // Racuna decimalna mesta
    for (int i = 0; i < precision && bufferSize > 1; i++) {
        decimalPart *= 10;
        int digit = (int)decimalPart;
        *buffer++ = '0' + digit;
        bufferSize--;
        decimalPart -= digit;
    }

    // Null-terminate the buffer
    *buffer++ = '\r';
    *buffer++ = '\n';
    *buffer = '\0';
}


void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart == &huart2){

		if(Rx_data[0] == 10){

			if (strcmp(Rx_data_buffer, "RESET") == 0){
				char Tx_data[10] = "OK\r\n";
				joules = 0.0;
				workTime = 0.0;
				size_t messageLength = strlen(Tx_data);

				HAL_UART_Transmit(&huart2, (uint8_t*)Tx_data, messageLength, HAL_MAX_DELAY);
			}

			else if (strcmp(Rx_data_buffer, "JOULE") == 0){
				char j_char[20];

				doubleToCharArray(joules, &j_char, 20);

				HAL_UART_Transmit(&huart2, (uint8_t*)j_char, strlen(j_char), HAL_MAX_DELAY);
			}
			else if (strcmp(Rx_data_buffer, "TALL") == 0){
				__NOP();
			}
			else if (strcmp(Rx_data_buffer, "TTRANS") == 0){
				char t_char[20];

				doubleToCharArray(workTime, &t_char, 20);

				HAL_UART_Transmit(&huart2, (uint8_t*)t_char, strlen(t_char), HAL_MAX_DELAY);
			}

			// Pocisti buffer, ter resetira stevec
			memset(Rx_data_buffer, 0, sizeof(Rx_data_buffer));
			Rx_data_index = 0;
		}
		else if(Rx_data[0] != 13 && Rx_data[0] != 245){
			Rx_data_buffer[Rx_data_index] = Rx_data[0];
			Rx_data_index ++;
		}


		HAL_UART_Receive_IT(&huart2, Rx_data, 1);
	    }

}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
