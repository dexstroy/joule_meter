# Power Consumption Meter

## About
Power consumption meter that measures voltage drop over a shunt resistor and calculates power consumption in joules. The measurements are displayed on a screen.

We used the STM32F103C8 microcontroller, which periodically took analog samples using DMA technology.
![Local Image](images/merilnaNaprava.jpg)

The meter can be controlled over UART, allowing you to read measurements and reset values.

This power consumption meter was utilized for automatic tests and measurements on an NB-IoT communication module.
![Local Image](images/merilnaPriprava.jpg)

![Local Image](images/merilnaPripravaCelota.png)
